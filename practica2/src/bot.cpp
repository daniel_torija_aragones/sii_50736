#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#include "DatosMemCompartida.h"


int main(){

	DatosMemCompartida *memcomp;
	int bottxt;

	bottxt=open("/tmp/bot", O_RDWR);
	if(bottxt==-1){ perror("Error en la apertura de fichero");
		return 1;}
	memcomp=(DatosMemCompartida*) mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE,MAP_SHARED,bottxt,0);
	if(memcomp==MAP_FAILED){
	perror("Error en la proyeccion del fichero");
	close(bottxt);
	return 1;
	}
	
	close(bottxt);

	while(1){

		if(memcomp->raqueta1.y1 < memcomp->esfera.centro.y) memcomp->accion=1;
		else if(memcomp->raqueta1.y2 > memcomp->esfera.centro.y) memcomp->accion=-1;
				
		usleep(25000);
	
	}
	munmap(memcomp,sizeof(memcomp));
	return 0;
}
